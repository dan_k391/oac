// use camelCase for variable names and fucntion names
const variableName = 'variableName';

function functionName() {
    // do something
}

// use PascalCase for class names
class ClassName {
    // do something
}

// use double quotes for strings
const string = "string";

// use single quotes for characters (a token)
const character = 'c';
const token = '<>';

// always add semicolons at the end of a statement
const statement = 'statement';
