import { Scanner } from "./front/lex/scanner";
import { Parser } from "./front/parse/parser";
import { ASTNode, ProgramNode } from "./front/parse/ast";

class Interpreter {
    private input: string;

    constructor(input: string) {
        this.input = input;
    }

    interpret(): ProgramNode {
        const scanner = new Scanner(this.input);
        const tokens = scanner.scan();
        console.log(tokens);
        const parser = new Parser(tokens);
        const ast = parser.parse();
        console.log(ast);
        return ast;
    }

    runtime(): void {
        const ast = this.interpret();
    }
}

export {
    Interpreter
}
