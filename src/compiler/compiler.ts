import { Scanner } from "./front/lex/scanner";
import { Parser } from "./front/parse/parser";
import { Emitter } from "./back/emitter";


interface Environment {
    output: OutputFunction;
}

interface OutputFunction {
    (output: number | string): void;
}

class Compiler {
    private input: string;
    private env: Environment;

    constructor(input: string, env: Environment) {
        this.input = input;
        this.env = env;
    }

    compile(): Uint8Array {
        const scanner = new Scanner(this.input);
        const tokens = scanner.scan();
        console.log(tokens);
        const parser = new Parser(tokens);
        const ast = parser.parse();
        console.log(ast);
        const emitter = new Emitter(ast);
        const wasm = emitter.emit();
        console.log(wasm);
        return wasm;
    }

    /**
     * @return {number} The runtime taken
     */

    async runtime(): Promise<number> {
        const wasm = this.compile();
        const { instance } = await WebAssembly.instantiate(wasm, {
            env: {
                ...this.env
            }
        });
        console.log(instance);
        const run = instance.exports.run as CallableFunction;

        const start = new Date().getTime();
        run();
        const end = new Date().getTime();
        return end - start;
    }
}

export {
    Compiler
}
