import { Token, tokenType } from "../lex/token";


const enum nodeKind {
    ProgramNode,
    FuncDefNode,
    ProcDefNode,
    ReturnNode,
    VarDeclNode,
    ArrDeclNode,
    TypeDefNode,
    VarAssignNode,
    ArrAssignNode,
    IfNode,
    WhileNode,
    RepeatNode,
    ForNode,
    ExprStmtNode,
    VarExprNode,
    ArrExprNode,
    CallExprNode,
    UnaryExprNode,
    BinaryExprNode,
    NumberExprNode,
    StringExprNode,
    BoolExprNode,
    OutputNode,
    InputNode
}

abstract class ASTNode {
    constructor(public kind: nodeKind) { }

    public abstract toString(): string;
}

class Expr extends ASTNode {
    toString(): string {
        return "Expr";
    }
}

class Stmt extends ASTNode {
    toString(): string {
        return "Stmt";
    }
}

interface Param {
    ident: string;
    type: string;
}

interface Arg {
    ident: string;
    type: string;
}

class ProgramNode extends ASTNode {
    public body: Array<Stmt>;

    constructor(body: Array<Stmt>) {
        super(nodeKind.ProgramNode);
        this.body = body;
    }

    toString(): string {
        return "ProgramNode";
    }
}

class FuncDefNode extends Stmt {
    public ident: string;
    public params: Array<Param>;
    // change type
    public type: string;
    public body: Array<Stmt>;

    constructor(ident: string, params: Array<Param>, type: string, body: Array<ASTNode>) {
        super(nodeKind.FuncDefNode);
        this.ident = ident;
        this.params = params;
        this.type = type;
        this.body = body;
    }

    toString(): string {
        return "FuncDefNode";
    }
}

class ProcDefNode extends Stmt {
    public ident: string;
    public params: Array<Param>;
    public body: Array<Stmt>;

    constructor(ident: string, params: Array<Param>, body: Array<Stmt>) {
        super(nodeKind.ProcDefNode);
        this.ident = ident;
        this.params = params;
        this.body = body;
    }

    toString(): string {
        return "ProcDefNode";
    }
}

class ReturnNode extends Stmt {
    public expr: Expr;

    constructor(expr: Expr) {
        super(nodeKind.ReturnNode);
        this.expr = expr;
    }

    toString(): string {
        return "ReturnNode";
    }
}

class VarDeclNode extends Stmt {
    public ident: Token;
    public type: Token;

    constructor(ident: Token, type: Token) {
        super(nodeKind.VarDeclNode);
        this.ident = ident;
        this.type = type;
    }

    toString(): string {
        return "VarDeclNode";
    }
}

class ArrDeclNode extends Stmt {
    public ident: string;
    public type: string;
    public lower: number;
    public upper: number;

    constructor(ident: string, type: string, lower: number, upper: number) {
        super(nodeKind.ArrAssignNode);
        this.ident = ident;
        this.type = type;
        this.lower = lower;
        this.upper = upper;
    }

    toString(): string {
        return "ArrDeclNode";
    }
}

class TypeDefNode extends Stmt {
    public ident: string;
    public body: VarDeclNode;

    constructor(ident: string, body: VarDeclNode) {
        super(nodeKind.TypeDefNode);
        this.ident = ident;
        this.body = body;
    }

    toString(): string {
        return "TypeDefNode";
    }
}

class VarAssignNode extends Stmt {
    public ident: Token;
    public expr: Expr;

    constructor(ident: Token, expr: Expr) {
        super(nodeKind.VarAssignNode);
        this.ident = ident;
        this.expr = expr;
    }

    toString(): string {
        return "VarAssignNode";
    }
}

class ArrAssignNode extends Stmt {
    public ident: string;
    public index: number;
    public expr: Expr;

    constructor(ident: string, index: number, expr: Expr) {
        super(nodeKind.ArrAssignNode);
        this.ident = ident;
        this.index = index;
        this.expr = expr;
    }

    toString(): string {
        return "ArrAssignNode";
    }
}

class IfNode extends Stmt {
    public condition: Expr;
    public body: Array<Stmt>;
    public elseBody?: Array<Stmt>;

    constructor(condition: Expr, body: Array<Stmt>, elseBody?: Array<Stmt>) {
        super(nodeKind.IfNode);
        this.condition = condition;
        this.body = body;
        if (elseBody) {
            this.elseBody = elseBody;
        }
    }

    toString(): string {
        return "IfNode";
    }
}

class WhileNode extends Stmt {
    public condition: Expr;
    public body: Array<Stmt>;

    constructor(condition: Expr, body: Array<Stmt>) {
        super(nodeKind.WhileNode);
        this.condition = condition;
        this.body = body;
    }

    toString(): string {
        return "WhileNode";
    }
}

// post-condition loops
class RepeatNode extends Stmt {
    public body: Array<Stmt>;
    public condition: Expr;

    constructor(body: Array<Stmt>, condition: Expr) {
        super(nodeKind.RepeatNode);
        this.body = body;
        this.condition = condition;
    }

    toString(): string {
        return "RepeatNode";
    }
}

class ForNode extends Stmt {
    // the identifier of the variable
    public ident: Token;
    public start: Expr;
    public end: Expr;
    public step: Expr;
    public body: Array<Stmt>;

    constructor(ident: Token, start: Expr, end: Expr, step: Expr, body: Array<Stmt>) {
        super(nodeKind.ForNode);
        this.ident = ident;
        this.start = start;
        this.end = end;
        this.step = step;
        this.body = body;
    }

    toString(): string {
        return "ForNode";
    }
}

class ExprStmtNode extends Stmt {
    public expr: Expr;

    constructor(expr: Expr) {
        super(nodeKind.ExprStmtNode);
        this.expr = expr;
    }

    toString(): string {
        return "ExprStmtNode";
    }
}

class VarExprNode extends Expr {
    public ident: Token;

    constructor(ident: Token) {
        super(nodeKind.VarExprNode);
        this.ident = ident;
    }

    toString(): string {
        return "VarExprNode";
    }
}

class ArrExprNode extends Expr {
    public ident: string;
    public index: number;

    constructor(ident: string, index: number) {
        super(nodeKind.ArrExprNode);
        this.ident = ident;
        this.index = index;
    }

    toString(): string {
        return "ArrExprNode";
    }
}

class CallExprNode extends Expr {
    public ident: string;
    public args: Array<Arg>;

    constructor(ident: string, args: Array<Arg>) {
        super(nodeKind.CallExprNode);
        this.ident = ident;
        this.args = args;
    }

    toString(): string {
        return "CallExprNode";
    }
}

class UnaryExprNode extends Expr {
    public operator: Token;
    public expr: Expr;

    constructor(operator: Token, expr: Expr) {
        super(nodeKind.UnaryExprNode);
        this.operator = operator;
        this.expr = expr;
    }

    toString(): string {
        return "UnaryExprNode";
    }
}

class BinaryExprNode extends Expr {
    public left: Expr;
    public operator: Token;
    public right: Expr;

    constructor(left: Expr, operator: Token, right: Expr) {
        super(nodeKind.BinaryExprNode);
        this.left = left;
        this.operator = operator;
        this.right = right;
    }

    toString(): string {
        return "BinaryExprNode";
    }
}

// integers and reals are both numbers, currently
class NumberExprNode extends Expr {
    public value: number;

    constructor(value: number) {
        super(nodeKind.NumberExprNode);
        this.value = value;
    }

    toString(): string {
        return "NumberExprNode";
    }
}

class StringExprNode extends Expr {
    public value: string;

    constructor(value: string) {
        super(nodeKind.StringExprNode);
        this.value = value;
    }

    toString(): string {
        return "StringExprNode";
    }
}

class BoolExprNode extends Expr {
    public value: boolean;

    constructor(value: boolean) {
        super(nodeKind.BoolExprNode);
        this.value = value;
    }

    toString(): string {
        return "BoolExprNode";
    }
}

class OutputNode extends Stmt {
    public expr: Expr;

    constructor(expr: Expr) {
        super(nodeKind.OutputNode);
        this.expr = expr;
    }

    toString(): string {
        return "OutputNode";
    }
}

class InputNode extends Stmt {
    public ident: string;

    constructor(ident: string) {
        super(nodeKind.InputNode);
        this.ident = ident;
    }

    toString(): string {
        return "InputNode";
    }
}

export {
    nodeKind,

    ASTNode,
    Expr,
    Stmt,
    Param,
    Arg,
    ProgramNode,
    FuncDefNode,
    ProcDefNode,
    ReturnNode,
    VarDeclNode,
    ArrDeclNode,
    TypeDefNode,
    VarAssignNode,
    ArrAssignNode,
    IfNode,
    WhileNode,
    RepeatNode,
    ForNode,
    ExprStmtNode,
    VarExprNode,
    ArrExprNode,
    CallExprNode,
    UnaryExprNode,
    BinaryExprNode,
    NumberExprNode,
    StringExprNode,
    BoolExprNode,
    OutputNode,
    InputNode
};
