const enum valType {
    i32 = 0x7f,
    f32 = 0x7d,
    f64 = 0x7c
};

export {
    valType
}
