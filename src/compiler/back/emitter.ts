// TODO: Improve the code

import {
    nodeKind,
    ASTNode,
    Expr,
    Stmt,
    Param,
    Arg,
    ProgramNode,
    FuncDefNode,
    ProcDefNode,
    ReturnNode,
    VarDeclNode,
    ArrDeclNode,
    TypeDefNode,
    VarAssignNode,
    ArrAssignNode,
    IfNode,
    WhileNode,
    RepeatNode,
    ForNode,
    ExprStmtNode,
    VarExprNode,
    ArrExprNode,
    CallExprNode,
    UnaryExprNode,
    BinaryExprNode,
    NumberExprNode,
    StringExprNode,
    BoolExprNode,
    OutputNode,
    InputNode
} from "../front/parse/ast";
import {
    unsignedLEB128,
    signedLEB128,
    encodeString,
    ieee754
} from "./encoding";
import { RuntimeError } from "../error";
import { _Symbol } from "./symbol";
import { valType } from "./valtype";
import { tokenType } from "../front/lex/token";


const enum section {
    custom = 0,
    type = 1,
    import = 2,
    func = 3,
    table = 4,
    memory = 5,
    global = 6,
    export = 7,
    start = 8,
    element = 9,
    code = 10,
    data = 11
};

const enum blockType {
    void = 0x40
}

const enum opCode {
    block = 0x02,
    loop = 0x03,
    if = 0x04,
    else = 0x05,
    br = 0x0c,
    br_if = 0x0d,
    end = 0x0b,
    call = 0x10,
    get_local = 0x20,
    set_local = 0x21,
    i32_store_8 = 0x3a,
    i32_const = 0x41,
    f32_const = 0x43,
    f64_const = 0x44,
    i32_eqz = 0x45,
    i32_eq = 0x46,
    f32_eq = 0x5b,
    f32_lt = 0x5d,
    f32_gt = 0x5e,
    f64_eqz = 0x60,
    f64_eq = 0x61,
    f64_ne = 0x62,
    f64_lt = 0x63,
    f64_gt = 0x64,
    f64_le = 0x65,
    f64_ge = 0x66,
    i32_add = 0x6a,
    i32_sub = 0x6b,
    i32_mul = 0x6c,
    i32_div_s = 0x6d,
    i32_div_u = 0x6e,
    i32_and = 0x71,
    f32_add = 0x92,
    f32_sub = 0x93,
    f32_mul = 0x94,
    f32_div = 0x95,
    f64_add = 0xa0,
    f64_sub = 0xa1,
    f64_mul = 0xa2,
    f64_div = 0xa3,
    i32_trunc_f32_s = 0xa8
};

const arithInst = new Map<tokenType, opCode>(
    [
        [tokenType.PLUS, opCode.f64_add],
        [tokenType.MINUS, opCode.f64_sub],
        [tokenType.STAR, opCode.f64_mul],
        [tokenType.SLASH, opCode.f64_div]
    ]
)

const compInstr = new Map<tokenType, opCode>(
    [
        [tokenType.EQUAL, opCode.f64_eq],
        [tokenType.LESS_GREATER, opCode.f64_ne],
        [tokenType.LESS, opCode.f64_lt],
        [tokenType.GREATER, opCode.f64_gt],
        [tokenType.LESS_EQUAL, opCode.f64_le],
        [tokenType.GREATER_EQUAL, opCode.f64_ge]
    ]
);

const enum exportType {
    func = 0x00,
    table = 0x01,
    mem = 0x02,
    global = 0x03
};

const funcType = 0x60;
const emptyArray = 0x00;

const magicModuleHeader = [0x00, 0x61, 0x73, 0x6d];
const moduleVersion = [0x01, 0x00, 0x00, 0x00];

function flatten(arr: Array<any>): Array<any> {
    return arr.reduce((acc, val) => acc.concat(val), []);
}

function encodeVector(data: Array<any>): Array<any> {
    return [
        ...unsignedLEB128(data.length),
        ...flatten(data)
    ];
}

function encodeLocal(count: number, type: valType): Array<number> {
    return [
        ...unsignedLEB128(count),
        type
    ];
}

function createSection(sectionType: section, sectionData: Array<any>): Array<any> {
    return [
        sectionType, 
        ...encodeVector(sectionData),
    ]
}

class CodeGenerator {
    private ast: ProgramNode;
    private code: Array<number>;
    private symbols: Map<string, _Symbol>;

    constructor(ast: ProgramNode) {
        this.ast = ast;
        this.code = new Array<number>();
        this.symbols = new Map<string, _Symbol>();
    }

    public generate(): Array<number> {
        this.emitStatements(this.ast.body);

        // local declarations
        const locals: Array<Array<number>> = [];
        for (const symbol of this.symbols.values()) {
            locals.push(encodeLocal(1, symbol.type));
        }
        console.log(this.code);

        return encodeVector([...encodeVector(locals), ...this.code, opCode.end]);
    }

    private setlocalIndexForSymbol(name: string, type: valType): number {
        if (!this.symbols.has(name)) {
            this.symbols.set(name, new _Symbol(this.symbols.size, type));
        }
        return this.getlocalIndexForSymbol(name);
    }

    private getlocalIndexForSymbol(name: string): number {
        if (!this.symbols.has(name)) {
            throw new RuntimeError("Symbol '" +  name + "' is not declared");
        }
        return this.symbols.get(name)?.index as number;
    }

    private emitExpression(expression: Expr): void {
        switch (expression.kind) {
            case nodeKind.VarExprNode:
                this.varExpression(expression as VarExprNode);
                break;
            case nodeKind.UnaryExprNode:
                this.unaryExpression(expression as UnaryExprNode);
                break;
            case nodeKind.BinaryExprNode:
                this.binaryExpression(expression as BinaryExprNode);
                break;
            case nodeKind.NumberExprNode:
                this.numberExpression(expression as NumberExprNode);
                break;
            default:
                break;
        }
    }

    private varExpression(node: VarExprNode): void {
        this.code.push(opCode.get_local);
        this.code.push(...unsignedLEB128(this.getlocalIndexForSymbol(node.ident.lexeme)));
    }

    private unaryExpression(node: UnaryExprNode): void {
        // 0 + expr or 0 - expr
        this.code.push(opCode.f64_const);
        this.code.push(...ieee754(0));
        this.emitExpression(node.expr);
        switch (node.operator.type) {
            case tokenType.PLUS:
                this.code.push(opCode.f64_add);
                break;
            case tokenType.MINUS:                
                this.code.push(opCode.f64_sub);
                break;
            default:
                break;
        }
    }

    private binaryExpression(node: BinaryExprNode): void {
        this.emitExpression(node.left);
        this.emitExpression(node.right);

        if (arithInst.has(node.operator.type)) {
            this.code.push(arithInst.get(node.operator.type) as opCode);
        } 
        else if (compInstr.has(node.operator.type)) {
            this.code.push(compInstr.get(node.operator.type) as opCode);
        }
    }
    
    private numberExpression(node: NumberExprNode): void {
        this.code.push(opCode.f64_const);
        this.code.push(...ieee754(node.value));
    }

    private emitStatements(statements: Array<Stmt>) {
        for (const statement of statements) {
            this.emitStatement(statement);
        }
    }

    private emitStatement(statement: Stmt): void {
        switch (statement.kind) {
            case nodeKind.OutputNode:
                this.outputStatement(statement as OutputNode);
                break;
            case nodeKind.VarDeclNode:
                this.varDeclStatement(statement as VarDeclNode);
                break;
            case nodeKind.VarAssignNode:
                this.varAssignStatement(statement as VarAssignNode);
                break;
            case nodeKind.IfNode:
                this.ifStatement(statement as IfNode);
                break;
            case nodeKind.WhileNode:
                this.whileStatement(statement as WhileNode);
                break;
            case nodeKind.RepeatNode:
                this.repeatStatement(statement as RepeatNode);
                break;
            case nodeKind.ForNode:
                this.forStatement(statement as ForNode);
                break;
            default:
                break;
        }
    }

    private outputStatement(node: OutputNode): void {
        this.emitExpression(node.expr);
        this.code.push(opCode.call);
        this.code.push(...unsignedLEB128(0));
    }

    private varDeclStatement(node: VarDeclNode): void {
        let type = valType.f64;
        switch (node.type.type) {
            case tokenType.INTEGER:
                this.emitExpression(new NumberExprNode(0));
                break;
            case tokenType.REAL:
                this.emitExpression(new NumberExprNode(0));
                break;
            case tokenType.STRING:
                // improve latter
                type = valType.i32;
                break;
            default:
                break;
        }
        this.code.push(opCode.set_local);
        this.code.push(...unsignedLEB128(this.setlocalIndexForSymbol(node.ident.lexeme, type)));
    }

    private varAssignStatement(node: VarAssignNode): void {
        this.emitExpression(node.expr);
        this.code.push(opCode.set_local);
        this.code.push(...unsignedLEB128(this.getlocalIndexForSymbol(node.ident.lexeme)));
    }

    private ifStatement(node: IfNode): void {
        // if block
        this.emitExpression(node.condition);
        this.code.push(opCode.if);
        this.code.push(blockType.void);
        this.emitStatements(node.body);
 
        // else block
        if (node.elseBody) {
            this.code.push(opCode.else);
            this.emitStatements(node.elseBody);
        }
        this.code.push(opCode.end);
    }

    private whileStatement(node: WhileNode): void {
        this.code.push(opCode.loop);
        this.code.push(blockType.void);
        this.emitExpression(node.condition);
        this.code.push(opCode.if);
        this.code.push(blockType.void);
        this.emitStatements(node.body);
        this.code.push(opCode.br);
        this.code.push(...unsignedLEB128(1));
        this.code.push(opCode.end);
        this.code.push(opCode.end);
    }

    private repeatStatement(node: RepeatNode): void {
        this.code.push(opCode.loop);
        this.code.push(blockType.void);
        this.emitStatements(node.body);
        this.emitExpression(node.condition);
        // if condition is not true 😂
        this.code.push(opCode.i32_eqz);
        this.code.push(opCode.br_if);
        // label index -> u32
        this.code.push(...unsignedLEB128(0));
        this.code.push(opCode.end);
    }

    private forStatement(node: ForNode): void {
        this.emitExpression(node.start);
        this.code.push(opCode.set_local);
        this.code.push(...unsignedLEB128(this.setlocalIndexForSymbol(node.ident.lexeme, valType.f64)));
        this.code.push(opCode.loop);
        this.code.push(blockType.void);
        this.code.push(opCode.get_local);
        this.code.push(...unsignedLEB128(this.getlocalIndexForSymbol(node.ident.lexeme)));
        this.emitExpression(node.end);
        this.code.push(opCode.f64_le);
        this.code.push(opCode.if);
        this.code.push(blockType.void);
        this.emitStatements(node.body);
        this.code.push(opCode.get_local);
        this.code.push(...unsignedLEB128(this.getlocalIndexForSymbol(node.ident.lexeme)));
        this.emitExpression(node.step);
        this.code.push(opCode.f64_add);
        this.code.push(opCode.set_local);
        this.code.push(...unsignedLEB128(this.getlocalIndexForSymbol(node.ident.lexeme)));
        this.code.push(opCode.br);
        this.code.push(...unsignedLEB128(1));
        this.code.push(opCode.end);
        this.code.push(opCode.end);
    }
}

class Emitter {
    private ast: ProgramNode;

    constructor(ast: ProgramNode) {
        this.ast = ast;
    }

    public emit(): Uint8Array {
        const generator = new CodeGenerator(this.ast);
        
        const voidVoidType = [funcType, emptyArray, emptyArray];
        const doubleVoidType = [
            funcType,
            ...encodeVector([valType.f64]),
            emptyArray
        ];

        const typeSection = createSection(
            section.type,
            encodeVector([voidVoidType, doubleVoidType])
        );
        const funcSection = createSection(
            section.func,
            encodeVector([0x00])
        );
        const outputFunctionImport = [
            ...encodeString("env"),
            ...encodeString("output"),
            exportType.func,
            // type index
            0x01
        ];
        const importSection = createSection(
            section.import,
            encodeVector([outputFunctionImport])
        );
        const exportSection = createSection(
            section.export,
            encodeVector(
                // 0x01 is the index of the function, not type
                [[...encodeString("run"), exportType.func, 0x01]]
            )
        );
        const code = generator.generate();
        const codeSection = createSection(
            section.code,
            encodeVector([code])
        );

        return Uint8Array.from([
            ...magicModuleHeader,
            ...moduleVersion,
            ...typeSection,
            ...importSection,
            ...funcSection,
            ...exportSection,
            ...codeSection
        ]);
    }
}

export {
    Emitter
}
