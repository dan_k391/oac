Date : 2022-12-01 12:45:13
Directory : d:\web\pseudocode-ide\src
Total : 21 files,  1620 codes, 57 comments, 305 blanks, all 1982 lines

Languages
+------------+------------+------------+------------+------------+------------+
| language   | files      | code       | comment    | blank      | total      |
+------------+------------+------------+------------+------------+------------+
| TypeScript |         16 |      1,303 |         57 |        252 |      1,612 |
| Vue        |          3 |        287 |          0 |         44 |        331 |
| BNF        |          1 |         22 |          0 |          7 |         29 |
| SCSS       |          1 |          8 |          0 |          2 |         10 |
+------------+------------+------------+------------+------------+------------+

Directories
+----------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                           | files      | code       | comment    | blank      | total      |
+----------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                              |         21 |      1,620 |         57 |        305 |      1,982 |
| compiler                                                       |         12 |      1,280 |         49 |        244 |      1,573 |
| compiler\back                                                  |          4 |        373 |         11 |         54 |        438 |
| compiler\front                                                 |          5 |        806 |         35 |        166 |      1,007 |
| compiler\front\lex                                             |          2 |        226 |         20 |         47 |        293 |
| compiler\front\parse                                           |          3 |        580 |         15 |        119 |        714 |
| components                                                     |          2 |        191 |          0 |         30 |        221 |
| router                                                         |          1 |         14 |          0 |          4 |         18 |
| store                                                          |          1 |          8 |          0 |          2 |         10 |
| styles                                                         |          1 |          8 |          0 |          2 |         10 |
| styles\element                                                 |          1 |          8 |          0 |          2 |         10 |
+----------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+----------------------------------------------------------------+------------+------------+------------+------------+------------+
| filename                                                       | language   | code       | comment    | blank      | total      |
+----------------------------------------------------------------+------------+------------+------------+------------+------------+
| d:\web\pseudocode-ide\src\App.vue                              | Vue        |         96 |          0 |         14 |        110 |
| d:\web\pseudocode-ide\src\compiler\back\emitter.ts             | TypeScript |        308 |          7 |         42 |        357 |
| d:\web\pseudocode-ide\src\compiler\back\encoding.ts            | TypeScript |         45 |          3 |          6 |         54 |
| d:\web\pseudocode-ide\src\compiler\back\symbol.ts              | TypeScript |         12 |          1 |          4 |         17 |
| d:\web\pseudocode-ide\src\compiler\back\valtype.ts             | TypeScript |          8 |          0 |          2 |         10 |
| d:\web\pseudocode-ide\src\compiler\compiler.ts                 | TypeScript |         46 |          3 |         11 |         60 |
| d:\web\pseudocode-ide\src\compiler\error.ts                    | TypeScript |         31 |          0 |          7 |         38 |
| d:\web\pseudocode-ide\src\compiler\front\lex\scanner.ts        | TypeScript |        184 |         15 |         38 |        237 |
| d:\web\pseudocode-ide\src\compiler\front\lex\token.ts          | TypeScript |         42 |          5 |          9 |         56 |
| d:\web\pseudocode-ide\src\compiler\front\parse\ast.ts          | TypeScript |        361 |          3 |         80 |        444 |
| d:\web\pseudocode-ide\src\compiler\front\parse\parser.ts       | TypeScript |        197 |         12 |         32 |        241 |
| d:\web\pseudocode-ide\src\compiler\front\parse\pseudocode.ebnf | BNF        |         22 |          0 |          7 |         29 |
| d:\web\pseudocode-ide\src\compiler\interpreter.ts              | TypeScript |         24 |          0 |          6 |         30 |
| d:\web\pseudocode-ide\src\components\MonacoEditor.vue          | Vue        |         92 |          0 |         16 |        108 |
| d:\web\pseudocode-ide\src\components\XTerm.vue                 | Vue        |         99 |          0 |         14 |        113 |
| d:\web\pseudocode-ide\src\main.ts                              | TypeScript |         11 |          1 |          3 |         15 |
| d:\web\pseudocode-ide\src\router\index.ts                      | TypeScript |         14 |          0 |          4 |         18 |
| d:\web\pseudocode-ide\src\shims-vue.d.ts                       | TypeScript |          5 |          1 |          1 |          7 |
| d:\web\pseudocode-ide\src\standards.ts                         | TypeScript |          7 |          6 |          5 |         18 |
| d:\web\pseudocode-ide\src\store\index.ts                       | TypeScript |          8 |          0 |          2 |         10 |
| d:\web\pseudocode-ide\src\styles\element\index.scss            | SCSS       |          8 |          0 |          2 |         10 |
| Total                                                          |            |      1,620 |         57 |        305 |      1,982 |
+----------------------------------------------------------------+------------+------------+------------+------------+------------+