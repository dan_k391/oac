# Details

Date : 2022-12-01 12:45:13

Directory d:\\web\\pseudocode-ide\\src

Total : 21 files,  1620 codes, 57 comments, 305 blanks, all 1982 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.vue](/src/App.vue) | Vue | 96 | 0 | 14 | 110 |
| [src/compiler/back/emitter.ts](/src/compiler/back/emitter.ts) | TypeScript | 308 | 7 | 42 | 357 |
| [src/compiler/back/encoding.ts](/src/compiler/back/encoding.ts) | TypeScript | 45 | 3 | 6 | 54 |
| [src/compiler/back/symbol.ts](/src/compiler/back/symbol.ts) | TypeScript | 12 | 1 | 4 | 17 |
| [src/compiler/back/valtype.ts](/src/compiler/back/valtype.ts) | TypeScript | 8 | 0 | 2 | 10 |
| [src/compiler/compiler.ts](/src/compiler/compiler.ts) | TypeScript | 46 | 3 | 11 | 60 |
| [src/compiler/error.ts](/src/compiler/error.ts) | TypeScript | 31 | 0 | 7 | 38 |
| [src/compiler/front/lex/scanner.ts](/src/compiler/front/lex/scanner.ts) | TypeScript | 184 | 15 | 38 | 237 |
| [src/compiler/front/lex/token.ts](/src/compiler/front/lex/token.ts) | TypeScript | 42 | 5 | 9 | 56 |
| [src/compiler/front/parse/ast.ts](/src/compiler/front/parse/ast.ts) | TypeScript | 361 | 3 | 80 | 444 |
| [src/compiler/front/parse/parser.ts](/src/compiler/front/parse/parser.ts) | TypeScript | 197 | 12 | 32 | 241 |
| [src/compiler/front/parse/pseudocode.ebnf](/src/compiler/front/parse/pseudocode.ebnf) | BNF | 22 | 0 | 7 | 29 |
| [src/compiler/interpreter.ts](/src/compiler/interpreter.ts) | TypeScript | 24 | 0 | 6 | 30 |
| [src/components/MonacoEditor.vue](/src/components/MonacoEditor.vue) | Vue | 92 | 0 | 16 | 108 |
| [src/components/XTerm.vue](/src/components/XTerm.vue) | Vue | 99 | 0 | 14 | 113 |
| [src/main.ts](/src/main.ts) | TypeScript | 11 | 1 | 3 | 15 |
| [src/router/index.ts](/src/router/index.ts) | TypeScript | 14 | 0 | 4 | 18 |
| [src/shims-vue.d.ts](/src/shims-vue.d.ts) | TypeScript | 5 | 1 | 1 | 7 |
| [src/standards.ts](/src/standards.ts) | TypeScript | 7 | 6 | 5 | 18 |
| [src/store/index.ts](/src/store/index.ts) | TypeScript | 8 | 0 | 2 | 10 |
| [src/styles/element/index.scss](/src/styles/element/index.scss) | SCSS | 8 | 0 | 2 | 10 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)