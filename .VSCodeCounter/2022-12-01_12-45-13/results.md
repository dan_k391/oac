# Summary

Date : 2022-12-01 12:45:13

Directory d:\\web\\pseudocode-ide\\src

Total : 21 files,  1620 codes, 57 comments, 305 blanks, all 1982 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 16 | 1,303 | 57 | 252 | 1,612 |
| Vue | 3 | 287 | 0 | 44 | 331 |
| BNF | 1 | 22 | 0 | 7 | 29 |
| SCSS | 1 | 8 | 0 | 2 | 10 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 21 | 1,620 | 57 | 305 | 1,982 |
| compiler | 12 | 1,280 | 49 | 244 | 1,573 |
| compiler\\back | 4 | 373 | 11 | 54 | 438 |
| compiler\\front | 5 | 806 | 35 | 166 | 1,007 |
| compiler\\front\\lex | 2 | 226 | 20 | 47 | 293 |
| compiler\\front\\parse | 3 | 580 | 15 | 119 | 714 |
| components | 2 | 191 | 0 | 30 | 221 |
| router | 1 | 14 | 0 | 4 | 18 |
| store | 1 | 8 | 0 | 2 | 10 |
| styles | 1 | 8 | 0 | 2 | 10 |
| styles\\element | 1 | 8 | 0 | 2 | 10 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)