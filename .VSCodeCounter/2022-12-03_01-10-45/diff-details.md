# Diff Details

Date : 2022-12-03 01:10:45

Directory d:\\web\\pseudocode-ide\\src

Total : 9 files,  140 codes, 5 comments, 16 blanks, all 161 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.vue](/src/App.vue) | Vue | 0 | 0 | 1 | 1 |
| [src/compiler/back/emitter.ts](/src/compiler/back/emitter.ts) | TypeScript | 76 | 2 | 3 | 81 |
| [src/compiler/front/lex/scanner.ts](/src/compiler/front/lex/scanner.ts) | TypeScript | 2 | 0 | 0 | 2 |
| [src/compiler/front/lex/token.ts](/src/compiler/front/lex/token.ts) | TypeScript | 1 | 0 | 0 | 1 |
| [src/compiler/front/parse/ast.ts](/src/compiler/front/parse/ast.ts) | TypeScript | 14 | 1 | 3 | 18 |
| [src/compiler/front/parse/parser.ts](/src/compiler/front/parse/parser.ts) | TypeScript | 43 | 1 | 8 | 52 |
| [src/compiler/front/parse/pseudocode.ebnf](/src/compiler/front/parse/pseudocode.ebnf) | BNF | 1 | 0 | 0 | 1 |
| [src/components/MonacoEditor.vue](/src/components/MonacoEditor.vue) | Vue | 1 | 0 | 0 | 1 |
| [src/standards.ts](/src/standards.ts) | TypeScript | 2 | 1 | 1 | 4 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details