# Diff Summary

Date : 2022-12-03 01:10:45

Directory d:\\web\\pseudocode-ide\\src

Total : 9 files,  140 codes, 5 comments, 16 blanks, all 161 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 6 | 138 | 5 | 15 | 158 |
| Vue | 2 | 1 | 0 | 1 | 2 |
| BNF | 1 | 1 | 0 | 0 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 9 | 140 | 5 | 16 | 161 |
| compiler | 6 | 137 | 4 | 14 | 155 |
| compiler\\back | 1 | 76 | 2 | 3 | 81 |
| compiler\\front | 5 | 61 | 2 | 11 | 74 |
| compiler\\front\\lex | 2 | 3 | 0 | 0 | 3 |
| compiler\\front\\parse | 3 | 58 | 2 | 11 | 71 |
| components | 1 | 1 | 0 | 0 | 1 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)