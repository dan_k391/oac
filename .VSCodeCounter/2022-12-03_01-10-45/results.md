# Summary

Date : 2022-12-03 01:10:45

Directory d:\\web\\pseudocode-ide\\src

Total : 21 files,  1760 codes, 62 comments, 321 blanks, all 2143 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 16 | 1,441 | 62 | 267 | 1,770 |
| Vue | 3 | 288 | 0 | 45 | 333 |
| BNF | 1 | 23 | 0 | 7 | 30 |
| SCSS | 1 | 8 | 0 | 2 | 10 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 21 | 1,760 | 62 | 321 | 2,143 |
| compiler | 12 | 1,417 | 53 | 258 | 1,728 |
| compiler\\back | 4 | 449 | 13 | 57 | 519 |
| compiler\\front | 5 | 867 | 37 | 177 | 1,081 |
| compiler\\front\\lex | 2 | 229 | 20 | 47 | 296 |
| compiler\\front\\parse | 3 | 638 | 17 | 130 | 785 |
| components | 2 | 192 | 0 | 30 | 222 |
| router | 1 | 14 | 0 | 4 | 18 |
| store | 1 | 8 | 0 | 2 | 10 |
| styles | 1 | 8 | 0 | 2 | 10 |
| styles\\element | 1 | 8 | 0 | 2 | 10 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)