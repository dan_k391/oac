# Details

Date : 2022-12-03 01:10:45

Directory d:\\web\\pseudocode-ide\\src

Total : 21 files,  1760 codes, 62 comments, 321 blanks, all 2143 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.vue](/src/App.vue) | Vue | 96 | 0 | 15 | 111 |
| [src/compiler/back/emitter.ts](/src/compiler/back/emitter.ts) | TypeScript | 384 | 9 | 45 | 438 |
| [src/compiler/back/encoding.ts](/src/compiler/back/encoding.ts) | TypeScript | 45 | 3 | 6 | 54 |
| [src/compiler/back/symbol.ts](/src/compiler/back/symbol.ts) | TypeScript | 12 | 1 | 4 | 17 |
| [src/compiler/back/valtype.ts](/src/compiler/back/valtype.ts) | TypeScript | 8 | 0 | 2 | 10 |
| [src/compiler/compiler.ts](/src/compiler/compiler.ts) | TypeScript | 46 | 3 | 11 | 60 |
| [src/compiler/error.ts](/src/compiler/error.ts) | TypeScript | 31 | 0 | 7 | 38 |
| [src/compiler/front/lex/scanner.ts](/src/compiler/front/lex/scanner.ts) | TypeScript | 186 | 15 | 38 | 239 |
| [src/compiler/front/lex/token.ts](/src/compiler/front/lex/token.ts) | TypeScript | 43 | 5 | 9 | 57 |
| [src/compiler/front/parse/ast.ts](/src/compiler/front/parse/ast.ts) | TypeScript | 375 | 4 | 83 | 462 |
| [src/compiler/front/parse/parser.ts](/src/compiler/front/parse/parser.ts) | TypeScript | 240 | 13 | 40 | 293 |
| [src/compiler/front/parse/pseudocode.ebnf](/src/compiler/front/parse/pseudocode.ebnf) | BNF | 23 | 0 | 7 | 30 |
| [src/compiler/interpreter.ts](/src/compiler/interpreter.ts) | TypeScript | 24 | 0 | 6 | 30 |
| [src/components/MonacoEditor.vue](/src/components/MonacoEditor.vue) | Vue | 93 | 0 | 16 | 109 |
| [src/components/XTerm.vue](/src/components/XTerm.vue) | Vue | 99 | 0 | 14 | 113 |
| [src/main.ts](/src/main.ts) | TypeScript | 11 | 1 | 3 | 15 |
| [src/router/index.ts](/src/router/index.ts) | TypeScript | 14 | 0 | 4 | 18 |
| [src/shims-vue.d.ts](/src/shims-vue.d.ts) | TypeScript | 5 | 1 | 1 | 7 |
| [src/standards.ts](/src/standards.ts) | TypeScript | 9 | 7 | 6 | 22 |
| [src/store/index.ts](/src/store/index.ts) | TypeScript | 8 | 0 | 2 | 10 |
| [src/styles/element/index.scss](/src/styles/element/index.scss) | SCSS | 8 | 0 | 2 | 10 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)